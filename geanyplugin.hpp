//
// geanyplugin.hpp - Simplified Geany C++ plugins
// Copyright (C) 2019 Matthew Brush <matt@geany.org>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
//

#ifndef GEANYPLUGIN_HPP
#define GEANYPLUGIN_HPP

extern "C" {
#include <geanyplugin.h>
#undef geany
}

/// All C++ symbols live inside the @a geany namespace.
namespace geany {

/// The abstract base class from which plugins must derive.
class plugin {
public:
  /// Virtual destructor.
  /// Does nothing in the abstract base class.
  virtual ~plugin() = default;

  /// Called when the plugin is enabled or at startup.
  /// This function is pure virtual and so has to be implemented by
  /// any derived classes.
  /// @return `true` on success or `false` on failure.
  virtual bool init() = 0;

  /// Called when the plugin is disabled or at shutdown.
  virtual void cleanup() {}

  /// Called when Geany is about to show the plugin preferences dialog.
  /// This allows plugins to put their own preferences page inside
  /// the Plugin Preferences dialog.
  /// @param dialog The top-level dialog box for the preferences.
  /// @return A `GtkWidget` containing the plugin's preferences widgets
  /// which will be packed in the plugin preferences dialog.
  virtual GtkWidget *configure(G_GNUC_UNUSED GtkDialog *dialog) {
    return gtk_label_new(_("This plugin provides no preferences."));
  }

  /// Called when the plugin needs to show the user some help text.
  /// For example open a URL in the browser or some text in a new document.
  virtual void help() {}

  // Get the plugin's name.
  const char *name() const noexcept { return plug.info->name; }

  // Get the plugin's description.
  const char *description() const noexcept { return plug.info->description; }

  // Get the plugin's version string.
  const char *version() const noexcept { return plug.info->version; }

  // Get the plugin's author.
  const char *author() const noexcept { return plug.info->author; }

protected:
  /// Reference to Geany's internal plugin structure for this plugin.
  GeanyPlugin &plug;

  /// Mandatory constructor.
  /// This must be called to instantiate a plugin with a valid reference to the
  /// GeanyPlugin instance, so any derived classes must "chain up" from their
  /// constructor to this one.
  /// @param plug Reference to the GeanyPlugin instance for this plugin.
  plugin(GeanyPlugin &plug) : plug(plug) {}
};

} // namespace geany

#include <type_traits>

/// Preprocessor function-like macro to simplify defining a C++ function.
/// @param min_api_version_ The minimum required API version of Geany, or @a
/// GEANY_API_VERSION to use whatever version the @a geanyplugin.h defines.
/// @param plug_name_ The name of the plugin (will be wrapped in `_()` for
/// translation).
/// @param plug_description_ A description of the plugin (will be wrapped in
/// `_()` for translation).
/// @param plug_version_ The version string of the plugin.
/// @param plug_author_ The author(s) of the plugin.
/// @param plugin_class_name_ The name of the C++ class which implements the
/// plugin.
/// @code
/// class my_plugin : public geany::plugin { ... };
/// GEANY_DEFINE_CXX_PLUGIN(
///   GEANY_API_VERSION,
///   "MyPlugin",
///   "Some plugin",
///   "0.1",
///   "John Smith <jsmith@domain.com>",
///   my_plugin
/// )
/// @endcode
#define GEANY_DEFINE_CXX_PLUGIN(min_api_version_, plug_name_,                  \
    plug_description_, plug_version_, plug_author_, plugin_class_name_)        \
  static_assert(std::is_base_of<geany::plugin, plugin_class_name_>::value,     \
      "class '" #plugin_class_name_ "' must derive from 'geanypp::plugin'");   \
  static gboolean init_func_(GeanyPlugin *, gpointer pdata) noexcept {         \
    try {                                                                      \
      g_assert(pdata);                                                         \
      return static_cast<plugin_class_name_ *>(pdata)->init();                 \
    } catch (...) {                                                            \
      g_critical("unhandled C++ exception in function '%s'", G_STRFUNC);       \
      return FALSE;                                                            \
    }                                                                          \
  }                                                                            \
  static void cleanup_func_(GeanyPlugin *, gpointer pdata) noexcept {          \
    try {                                                                      \
      g_assert(pdata);                                                         \
      static_cast<plugin_class_name_ *>(pdata)->cleanup();                     \
    } catch (...) {                                                            \
      g_critical("unhandled C++ exception in function '%s'", G_STRFUNC);       \
    }                                                                          \
  }                                                                            \
  static GtkWidget *configure_func_(                                           \
      GeanyPlugin *, GtkDialog *dialog, gpointer pdata) noexcept {             \
    try {                                                                      \
      g_assert(pdata);                                                         \
      return static_cast<plugin_class_name_ *>(pdata)->configure(dialog);      \
    } catch (...) {                                                            \
      g_critical("unhandled C++ exception in function '%s'", G_STRFUNC);       \
      return NULL;                                                             \
    }                                                                          \
  }                                                                            \
  static void help_func_(GeanyPlugin *, gpointer pdata) noexcept {             \
    try {                                                                      \
      g_assert(pdata);                                                         \
      static_cast<plugin_class_name_ *>(pdata)->help();                        \
    } catch (...) {                                                            \
      g_critical("unhandled C++ exception in function '%s'", G_STRFUNC);       \
    }                                                                          \
  }                                                                            \
  static void delete_func_(gpointer pdata) noexcept {                          \
    try {                                                                      \
      g_assert(pdata);                                                         \
      delete static_cast<plugin_class_name_ *>(pdata);                         \
    } catch (...) {                                                            \
      g_critical("unhandled C++ exception in function '%s'", G_STRFUNC);       \
    }                                                                          \
  }                                                                            \
  extern "C" {                                                                 \
  G_MODULE_EXPORT                                                              \
  void geany_load_module(GeanyPlugin *plug) {                                  \
    g_assert(plug);                                                            \
    plug->info->name = _(plug_name_);                                          \
    plug->info->description = _(plug_description_);                            \
    plug->info->version = plug_version_;                                       \
    plug->info->author = plug_author_;                                         \
    plug->funcs->init = init_func_;                                            \
    plug->funcs->cleanup = cleanup_func_;                                      \
    plug->funcs->configure = configure_func_;                                  \
    plug->funcs->help = help_func_;                                            \
    plugin_class_name_ *self = nullptr;                                        \
    try {                                                                      \
      self = new plugin_class_name_(*plug);                                    \
      if (!GEANY_PLUGIN_REGISTER_FULL(                                         \
              plug, min_api_version_, self, delete_func_)) {                   \
        delete self;                                                           \
      }                                                                        \
    } catch (...) {                                                            \
      delete self;                                                             \
      g_critical("unhandled C++ exception in function '%s'", G_STRFUNC);       \
    }                                                                          \
  }                                                                            \
  } // extern "C"

#endif // GEANYPLUGIN_HPP
