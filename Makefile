compiler_flags = \
	$(CPPFLAGS) \
	$(CXXFLAGS) \
	-fPIC -shared \
	-std=c++11 -Wall -Wextra -Werror \
	$(shell pkg-config --cflags geany)

linker_flags = \
	$(LDFLAGS) \
	$(shell pkg-config --libs geany)

all: geanypp-demo.so

clean:
	$(RM) geanypp-demo.so

install: geanypp-demo.so
	mkdir -p ~/.config/geany/plugins
	cp geanypp-demo.so ~/.config/geany/plugins

uninstall:
	rm -f ~/.config/geany/plugins/geanypp-demo.so

geanypp-demo.so: demo.cpp geanyplugin.hpp
	$(CXX) $(compiler_flags) -o $@ demo.cpp $(linker_flags)

.PHONY: all clean
