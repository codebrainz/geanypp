//
// demo.cpp - Example GeanyPP plugin
// Copyright (C) 2019 Matthew Brush <matt@geany.org>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
//

#include "geanyplugin.hpp"

#include <memory>

struct demo_plugin final : public geany::plugin {
  demo_plugin(GeanyPlugin &plug) : geany::plugin(plug) {}
  bool init() final {
    msgwin_status_add("Plugin '%s' enabled", name());
    return true;
  }
  void cleanup() final { msgwin_status_add("Plugin '%s' disabled", name()); }
  GtkWidget *configure(GtkDialog *) final {
    msgwin_status_add("Plugin '%s' configuration requested", name());
    auto message = std::unique_ptr<gchar, void (*)(gpointer)>(
        g_strdup_printf("Plugin '%s' provides no preferences", name()), g_free);
    return gtk_label_new(message.get());
  }
  void help() final {
    msgwin_status_add("Plugin '%s' help requested", name());
    utils_open_browser("https://gitlab.com/codebrainz/geanypp");
  }
};

GEANY_DEFINE_CXX_PLUGIN(GEANY_API_VERSION, "C++ Demo",
    "A simple C++ example plugin.", "0.1", "Me", demo_plugin)
